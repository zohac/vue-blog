export default class Api {
    static HOST = 'http://localhost';
    static PORT = '3000';
    static GET_POST = '/api/posts';

    static getBaseUrl () {
        let url = Api.HOST;

        if (Api.PORT) { url += ':' + Api.PORT }

        return url;
    }

    static getPost () {
        return Api.getBaseUrl() + Api.GET_POST;
    }
}