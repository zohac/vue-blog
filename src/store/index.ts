import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Api from '@/utils/api';
import { PostInterface } from "@/interfaces/PostInterface";

Vue.use(Vuex)
Vue.use(VueAxios, axios)

export default new Vuex.Store({
    state: {
        posts: []
    },
    getters: {
        posts: state => { return state.posts },
        getPostByUuid: (state) => (uuid: string) => {
            return state.posts.find((post: PostInterface) => post.uuid === uuid)
        }
    },
    mutations: {
        SET_POST (state, posts = []) {
            state.posts = posts
        }
    },
    actions: {
        async loadPosts ({state, dispatch }) {
            if (0 === state.posts.length) {
                await dispatch('refreshPosts')
            }
        },
        async refreshPosts ({ commit }) {
            axios
                .get(Api.getPost(), {})
                .then(response => {
                    commit('SET_POST', response.data);
                });
        }
    }
})