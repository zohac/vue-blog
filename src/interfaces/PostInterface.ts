export interface PostInterface {
    uuid: string;
    title: string;
    content: string;
}